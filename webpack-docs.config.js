const path = require('path');
const { DefinePlugin, HotModuleReplacementPlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const ENV = 'development';

const DEFAULT_DEV_SERVER_HOST = '0.0.0.0';
const DEFAULT_DEV_SERVER_PORT = 3000;

const DEV_SERVER_HOST = process.env.HOST || DEFAULT_DEV_SERVER_HOST;
const DEV_SERVER_PORT = process.env.PORT || DEFAULT_DEV_SERVER_PORT;

const AUI_DIST = path.resolve('node_modules/@atlassian/aui/dist');

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: {
        'aui-react': [
            'jquery',
            'react-hot-loader/patch',
            require.resolve('react-dev-utils/webpackHotDevClient'),
            './src/index.js',
            './docs/index.js'
        ]
    },

    output: {
        path: path.join(__dirname, 'dist'),
        filename: isProduction ? '[name].min.js' : '[name].js',
        library: 'AUI',
        libraryTarget: 'umd'
    },

    resolve: {
        alias: {
            jquery: 'jquery/src/jquery',
        }
    },

    plugins: [
        new DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(ENV)
            }
        }),

        new HotModuleReplacementPlugin(),

        new CopyWebpackPlugin([
            { from: AUI_DIST, to: 'assets' }
        ]),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: require('html-webpack-template'),
            title: 'AUI-REACT',
            appMountId: 'root',
            xhtml: true,
            mobile: true,
            inject: false,
            minify: {
                useShortDoctype: true,
                keepClosingSlash: true,
                collapseWhitespace: true,
                preserveLineBreaks: true
            }
        }),

        new HtmlWebpackIncludeAssetsPlugin({
            assets: [
                `assets/aui/css/aui.min.css`
            ],
            append: false,
            hash: true
        }),

        new HtmlWebpackIncludeAssetsPlugin({
            assets: [
                `assets/aui/js/aui.min.js`
            ],
            append: true,
            hash: true
        })
    ],

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: [
                    path.join(__dirname, 'src'),
                    path.join(__dirname, 'docs')
                ],
                use: [
                    {
                        loader: 'react-hot-loader/webpack'
                    },

                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true
                        }
                    }
                ]
            },

            {
                test: require.resolve('jquery'),
                use: [{
                    loader: 'expose-loader',
                    options: 'jQuery'
                }, {
                    loader: 'expose-loader',
                    options: '$'
                }]
            }
        ]
    },

    devServer: {
        contentBase: path.join(__dirname, './src'),
        compress: true,
        host: DEV_SERVER_HOST,
        port: DEV_SERVER_PORT,
        historyApiFallback: true,
        inline: true,
        hot: true
    }
};
