import React from 'react';

const AUIToggle = (...props) => (
    <aui-toggle {...props} />
);

export default AUIToggle;
