import React from 'react';
import PropTypes from 'prop-types';

const AUINav = ({ options, children }) => {
    let dataDrivenOptions;

    if (options) {
        dataDrivenOptions = options.map(option => {
            return (
                <li key={option.props.to}>
                    {option}
                </li>
            );
        });
    }

    return (
        <ul className="aui-nav">
            {dataDrivenOptions}
            {children}
        </ul>
    );
};

AUINav.propTypes = {
    onChange: PropTypes.func,
    options: PropTypes.array,
    selected: PropTypes.string,
    children: PropTypes.node
};

export default AUINav;
