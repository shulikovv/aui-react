import React from 'react';
import PropTypes from 'prop-types';

const AUIBreadcrumbs = ({ children }) => (
    <ol className="aui-nav aui-nav-breadcrumbs">
        {children}
    </ol>
);

AUIBreadcrumbs.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
    ])
};

export default AUIBreadcrumbs;
