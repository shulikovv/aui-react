const path = require('path');
const { DefinePlugin } = require('webpack');

module.exports = {
    devtool: 'source-map',

    entry: {
        'aui-react': './src/index.js'
    },

    plugins: [
        new DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
    ],

    externals: {
        'react': 'React',
        'prop-types': 'PropTypes'
    },

    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].min.js',
        publicPath: '/dist/',
        library: 'AUI',
        libraryTarget: 'umd'
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            loaders: 'babel-loader',
            include: path.join(__dirname, 'src')
        }]
    }
};
