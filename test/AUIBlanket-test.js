import React from 'react';
import { shallow } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import AUIBlanket from '../src/AUIBlanket';

chai.use(chaiEnzyme());

describe('AUIBlanket', () => {
    it('Should render correct mark up', () => {
        const wrapper = shallow(<AUIBlanket/>);
        expect(wrapper).to.have.tagName('div');
        expect(wrapper).to.have.className('aui-blanket');
        expect(wrapper).to.have.prop('aria-hidden', false);
        expect(wrapper).to.have.prop('tabIndex', '0');
        expect(wrapper).to.have.style('z-index', '2980');
    });

    it('Should render correct styles as specified', () => {
        const wrapper = shallow(<AUIBlanket style={{backgroundColor: 'red'}}/>);
        expect(wrapper).to.have.style('z-index', '2980');
        expect(wrapper).to.have.style('background-color', 'red');
    });

    it('should call the action function when the blanket is clicked', () => {
        const clickSpy = sinon.spy();
        const wrapper = shallow(<AUIBlanket onClick={clickSpy}/>);
        wrapper.simulate('click');
        expect(clickSpy).to.have.been.calledOnce;
    });
});
