import React from 'react';
import { shallow } from 'enzyme';

import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

import AUIApplicationLogo from '../src/AUIApplicationLogo';

describe('AUIApplicationLogo', () => {
    it('should render the correct markup', () => {
        expect(shallow(<AUIApplicationLogo />).html()).to.equal(`<h1 class="aui-header-logo" id="logo"><a href="/"><span class="aui-header-logo-device"></span></a></h1>`);
    });

    it('should render the correct class on the container for the confluence logo', () => {
        const wrapper = shallow(<AUIApplicationLogo logo="confluence" />);
        expect(wrapper).to.have.className('aui-header-logo aui-header-logo-confluence');
    });
});
